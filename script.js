 /*
Задание:
 Разработайте программу секундомер. 
 Секундомер должен иметь три кнопки Старт, Стоп, Сбросить. 
 Секундомер должен выводить время в формате 00:00:00 
 добавьте стили используя JS
*/

// set timer position
const container = get('.container');
const windowInnerHeight = window.innerHeight;
container.style.marginTop = `${Math.floor((windowInnerHeight / 2) - (container.offsetHeight / 2))}px`

// timer
function timerWork(){
    const 
        mnt = get('#mnt'), 
        sec = get('#sec'), 
        mSec = get('#m-sec'),
        btnStart = get("#btn-start"),
        btnPause = get('#btn-pause'),
        btnClear = get('#btn-clear'),
        timer = get('#timer');
    let counterMS = 0,
        counterS = 0,
        counterMin = 0;
     

    function start(){
        timer.style.color = 'black';
        let idSetInterval = setInterval(() => {
            counterMS < 10 ? mSec.innerHTML = `0${counterMS}` : mSec.innerHTML = `${counterMS}`;
            counterMS++;
            if(counterMS > 99){
                counterS++;
                counterMS = 0;
            }
            counterS < 10 ? sec.innerHTML = `0${counterS}` : sec.innerHTML = `${counterS}`;
            if(counterS > 59){
                counterMin++;
                counterS = 0;
            }
            counterMin < 10 ? mnt.innerHTML = `0${counterMin}` : mnt.innerHTML = `${counterMin}`;
            if(counterMin > 59){
                stopInterval();
                timer.innerHTML = '-------:(-------';
            }
        }, 10);

        function stopInterval() {
            timer.style.color = 'red';
            clearInterval(idSetInterval);
        }
    
        btnPause.addEventListener('click', stopInterval);
    }


    btnStart.addEventListener('click', start);
    btnClear.addEventListener('click', () => {
        timer.style.color = 'rgb(166, 166, 166)';
        counterMS = 0;
        counterS = 0;
        counterMin = 0;
        mSec.innerHTML = '00';
        sec.innerHTML = '00';
        mnt.innerHTML = '00';
    });

}

timerWork();

function get(elem) {
 return document.querySelector(elem);
}